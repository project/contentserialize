# Changelog

## [Unreleased]
### Added
- Test drush commands.
- Add a normalizer for link field items to replace node serial IDs with UUIDs.

### Changed
- Require PHP 7.x. 
- Require Drupal 8.7+.
- Extract normalizers into the _VCS Content Normalizers_ submodule.

### Removed
- Remove drush 8 support.
- Remove the menu link content entity normalizer (it's no longer needed with the
  link field item normalizer).

## [1.0-alpha2] - 2018-05-28
### Added
- Add drush 9 support.
- Add a utility class for dealing with traversables. 

### Changed
- Require Drupal 8.4+.
- Extract bulk entity loader into the _Bukle Entity API_ module.
- Use events to handle missing references to content entities.
- Add services and interfaces for major components.

## [1.0-alpha1] - 2017-11-20
Initial release of the module.

[Unreleased]: https://git.drupalcode.org/project/contentserialize/compare/8.x-1.0-alpha2...8.x-1.x
[1.0-alpha2]: https://git.drupalcode.org/project/contentserialize/compare/8.x-1.0-alpha1...8.x-1.0-alpha2
[1.0-alpha1]: https://git.drupalcode.org/project/contentserialize/tree/8.x-1.0-alpha1

<?php

namespace Drupal\vcsnormalizer\Normalizer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Normalizes/denormalizes content entities replacing serial IDs with UUIDs.
 */
class UuidContentEntityNormalizer extends NormalizerBase implements DenormalizerInterface {

  use SerializerAwareTrait {
    setSerializer as traitSetSerializer;
  }

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = ContentEntityInterface::class;

  /**
   * The content entity normalizer service.
   *
   * @var \Symfony\Component\Serializer\Normalizer\NormalizerInterface|
   *   \Symfony\Component\Serializer\Normalizer\DenormalizerInterface|
   *   \Symfony\Component\Serializer\SerializerAwareInterface
   */
  protected $entityNormalizer;

  /**
   * Create a new UUID content normalizer.
   *
   * @param \Symfony\Component\Serializer\Normalizer\NormalizerInterface $entity_normalizer
   *   The entity normalizer service.
   */
  public function __construct(NormalizerInterface $entity_normalizer) {
    $this->entityNormalizer = $entity_normalizer;
  }

  /**
   * {@inheritdoc}
   */
  public function setSerializer(SerializerInterface $serializer) {
    $this->traitSetSerializer($serializer);
    // Set the serializer on the contained normalizer as well.
    $this->entityNormalizer->setSerializer($serializer);
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = array()) {
    /** @var \Drupal\Core\Entity\EntityInterface $object */
    $attributes = $this->entityNormalizer->normalize($object, $format, $context);
    $keys = $object->getEntityType()->getKeys();
    foreach (['id', 'revision'] as $key_name) {
      unset($attributes[$keys[$key_name]]);
    }

    // Not all entity types that support FieldableEntityInterface have bundle
    // keys, eg. user and file.
    if (!empty($keys['bundle'])) {
      $attributes[$keys['bundle']] = $object->bundle();
    }

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $type, $format = NULL, array $context = []) {
    return $this->entityNormalizer->denormalize($data, $type, $format, $context);
  }

}

<?php

namespace Drupal\Tests\bulkentity\Kernel;

use Drupal\bulkentity\EntityLoader;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\entity_test\Entity\EntityTestMul;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Provides integration tests for the bulk entity loader.
 *
 * @group bulkentity
 */
class EntityLoaderTest extends EntityKernelTestBase {

  public static $modules = ['bulkentity'];

  /**
   * An array of test entity IDs.
   *
   * @var array
   */
  protected $ids;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    // Create an entity with the right entity type that should never be
    // returned.
    entity_test_create_bundle('entity_test_never_return');
    EntityTest::create([
      'type' => 'entity_test_never_return',
      'name' => 'Unreturned test entity',
    ])->save();
    // Create an entity of a different entity type that should never be
    // returned.
    $this->installEntitySchema('entity_test_mul');
    EntityTestMul::create()->save();

    // Create the target entities that should be returned.
    for ($i = 1; $i <= 6; $i++) {
      $values = [
        'name' => "Entity $i",
        'type' => 'entity_test',
      ];
      $entity = EntityTest::create($values);
      $entity->save();
      $this->ids[] = $entity->id();
    }
  }

  /**
   * Test that the correct entities are loaded by ID.
   */
  public function testLoadByIds() {
    $loader = new EntityLoader(\Drupal::entityTypeManager());
    $i = 1;
    foreach ($loader->byIds(2, $this->ids, 'entity_test') as $id => $entity) {
      $this->assertEquals("Entity $i", $entity->label());
      $i++;
    };
  }

  /**
   * Test that the correct entities are loaded by query.
   */
  public function testByQuery() {
    $query = \Drupal::entityQuery('entity_test')
      ->condition('type', 'entity_test')
      ->sort('name');
    $loader = new EntityLoader(\Drupal::entityTypeManager());
    $i = 1;
    foreach ($loader->byQuery(2, $query) as $entity) {
      $this->assertEquals("Entity $i", $entity->label());
      $i++;
    }
  }

  /**
   * Test that the correct entities are loaded by entity type and bundle.
   */
  public function testByEntityType() {
    $loader = new EntityLoader(\Drupal::entityTypeManager());
    $i = 1;
    foreach ($loader->byEntityType(2, 'entity_test', ['entity_test']) as $entity) {
      $this->assertEquals("Entity $i", $entity->label());
      $i++;
    }
  }

}

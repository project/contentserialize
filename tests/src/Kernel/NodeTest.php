<?php

namespace Drupal\Tests\contentserialize\Kernel;

use Drupal\contentserialize\Destination\FileDestination;
use Drupal\contentserialize\Source\FileSource;
use Drupal\node\Entity\Node;
use Drupal\Tests\contentserialize\Traits\NodeKernelTestTrait;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;

/**
 * Provides tests for serializing nodes.
 *
 * @group contentserialize
 */
class NodeTest extends KernelTestBase {

  use EntityReferenceTestTrait;
  use NodeKernelTestTrait;

  protected static $modules = ['node', 'entity_reference'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->setUpNode();
  }

  /**
   * Test entities referencing one another are exported and imported correctly.
   */
  public function testCyclicReferences() {
    $this->createContentType(['type' => 'article']);
    $this->createEntityReferenceField('node', 'article', 'field_related_content', 'Related content', 'node');

    // Create two nodes A and B that refer to each other.
    $a = Node::create([
      'type' => 'article',
      'title' => 'Test Content A',
      'body' => ['value' => 'Test Content A Body', 'format' => 'basic_html'],
      'uid' => 1,
    ]);
    $a->save();

    $b = Node::create([
      'type' => 'article',
      'title' => 'Test Content B',
      'body' => ['value' => 'Test Content B Body', 'format' => 'basic_html'],
      'uid' => 1,
    ]);
    // Is there a simpler way to set this, eg. just assignment of the entity?
    $b->field_related_content->target_id = $a->id();
    $b->save();

    $a->field_related_content->target_id = $b->id();
    $a->save();

    // Export them.

    $destination = new FileDestination($this->getContentDirectory());
    /** @var \Drupal\contentserialize\ExporterInterface $exporter */
    $exporter = \Drupal::service('contentserialize.exporter');
    $serialized = $exporter->exportMultiple([$a, $b], 'json', ['json_encode_options' => JSON_PRETTY_PRINT]);
    $destination->saveMultiple($serialized);

    // Delete them.
    $ids = [
      'uuid' => ['a' => $a->uuid(), 'b' => $b->uuid()],
      'id' => ['a' => $a->id(), 'b' => $b->id()],
      'vid' => ['a' => $a->vid, 'b' => $b->vid],
    ];

    $a->delete();
    $b->delete();

    // Reimport them.
    /** @var \Drupal\contentserialize\ImporterInterface $importer */
    $importer = \Drupal::service('contentserialize.importer');
    $result = $importer->import(new FileSource($this->getContentDirectory()));
    $nodes = \Drupal::entityTypeManager()->getStorage('node')
      ->loadByProperties(['uuid' => array_values($ids['uuid'])]);
    foreach ($nodes as $node) {
      $nodes[$node->uuid()] = $node;
    }
    $a = $nodes[$ids['uuid']['a']];
    $b = $nodes[$ids['uuid']['b']];

    // Check them.
    $this->assertEmpty($result->getFailures(), "There aren't any import errors.");

    $this->assertEquals($ids['uuid']['a'], $a->uuid());
    $this->assertEquals($ids['uuid']['b'], $b->uuid());

    $this->assertNotEquals($ids['id'], $a->id());
    $this->assertNotEquals($ids['id'], $b->id());

    $this->assertNotEquals($ids['vid'], $a->vid);
    $this->assertNotEquals($ids['vid'], $b->vid);

    $this->assertEquals($a->bundle(), 'article');
    $this->assertEquals($b->bundle(), 'article');

    $this->assertEquals('Test Content A', $a->label());
    $this->assertEquals('Test Content B', $b->label());

    $this->assertEquals('Test Content A Body', $a->body->value);
    $this->assertEquals('Test Content B Body', $b->body->value);

    $this->assertEquals($a->id(), $b->field_related_content->target_id);
    $this->assertEquals($b->id(), $a->field_related_content->target_id);

    $this->assertEquals(1, $a->uid->target_id);
    $this->assertEquals(1, $b->uid->target_id);
  }

}

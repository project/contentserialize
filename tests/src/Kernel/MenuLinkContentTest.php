<?php

namespace Drupal\Tests\contentserialize\Kernel;

use Drupal\contentserialize\Destination\FileDestination;
use Drupal\contentserialize\Source\FileSource;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\Tests\contentserialize\Traits\NodeKernelTestTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Provides tests for serializing menu link content entities.
 *
 * @group contentserialize
 */
class MenuLinkContentTest extends KernelTestBase {

  use NodeCreationTrait;
  use NodeKernelTestTrait;

  protected static $modules = ['menu_link_content', 'link'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->setUpNode();
    $this->installEntitySchema('menu_link_content');
  }

  /**
   * Test exporting and importing a link referencing a node.
   */
  public function testNodeLink() {
    $this->createContentType(['type' => 'page']);
    // The file source just returns in UUID order, so make sure the menu link
    // will be imported before its dependency (to test the fixer).
    $node = $this->createNode([
      'type' => 'page',
      'uuid' => 'e1c5eeb4-22a6-41a7-bcd2-d97c591c9a72',
    ]);

    $link = MenuLinkContent::create([
      'title' => 'Test Menu Link',
      'menu_name' => 'main',
      'bundle' => 'menu_link_content',
      'parent' => '',
      'link' => [['uri' => 'entity:node/' . $node->id()]],
      'uuid' => '03907dac-2923-41a5-8f4f-127e2db87712',
    ]);
    $link->save();

    // Export it.
    $destination = new FileDestination($this->getContentDirectory());
    /** @var \Drupal\contentserialize\ExporterInterface $exporter */
    $exporter = \Drupal::service('contentserialize.exporter');
    $serialized = $exporter->exportMultiple([$link, $node], 'json', ['json_encode_options' => JSON_PRETTY_PRINT]);
    $destination->saveMultiple($serialized);

    // Delete it.
    $link->delete();
    $node->delete();

    // Reimport it.
    /** @var \Drupal\contentserialize\ImporterInterface $importer */
    $importer = \Drupal::service('contentserialize.importer');
    $result = $importer->import(new FileSource($this->getContentDirectory()));

    /** @var \Drupal\Core\Entity\EntityRepositoryInterface $repository */
    $repository = \Drupal::service('entity.repository');
    /** @var \Drupal\file\FileInterface $file */
    $link = $repository->loadEntityByUuid('menu_link_content', '03907dac-2923-41a5-8f4f-127e2db87712');
    $node = $repository->loadEntityByUuid('node', 'e1c5eeb4-22a6-41a7-bcd2-d97c591c9a72');

    // Check it.
    $this->assertEmpty($result->getFailures(), "There aren't any import errors.");
    $this->assertEquals('03907dac-2923-41a5-8f4f-127e2db87712', $link->uuid());
    $this->assertEquals('e1c5eeb4-22a6-41a7-bcd2-d97c591c9a72', $node->uuid());
    $this->assertEquals('Test Menu Link', $link->label());
    $this->assertEquals('entity:node/' . $node->id(), $link->link->uri);
  }

}

<?php

namespace Drupal\Tests\contentserialize\Kernel;

use Drupal\contentserialize\Destination\FileDestination;
use Drupal\contentserialize\Source\FileSource;
use Drupal\file\Entity\File;
use Drupal\Tests\contentserialize\Traits\UserKernelTestTrait;

/**
 * Provides tests for serializing files.
 *
 * @group contentserialize
 */
class FileTest extends KernelTestBase {

  use UserKernelTestTrait;

  protected static $modules = ['file'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->setUpUser();
    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('file');
  }

  /**
   * Test exporting and importing a file.
   */
  public function testExportImport() {
    // Create a new file entity.
    $file = File::create([
      'uid' => 1,
      'filename' => 'drupal.txt',
      'uri' => $this->getContentDirectory() . '/drupal.txt',
      'filemime' => 'text/plain',
      'status' => FILE_STATUS_PERMANENT,
    ]);
    file_put_contents($file->getFileUri(), 'hello world');
    $file->save();

    // Export it.
    $path = $this->getContentDirectory();
    $destination = new FileDestination($path);
    /** @var \Drupal\contentserialize\ExporterInterface $exporter */
    $exporter = \Drupal::service('contentserialize.exporter');
    $serialized = $exporter->exportMultiple([$file], 'json', ['json_encode_options' => JSON_PRETTY_PRINT]);
    $destination->saveMultiple($serialized);

    // Delete it.
    $uuid = $file->uuid();
    $file->delete();

    // Reimport it.
    /** @var \Drupal\contentserialize\ImporterInterface $importer */
    $importer = \Drupal::service('contentserialize.importer');
    $result = $importer->import(new FileSource($path));

    /** @var \Drupal\Core\Entity\EntityRepositoryInterface $repository */
    $repository = \Drupal::service('entity.repository');
    /** @var \Drupal\file\FileInterface $file */
    $file = $repository->loadEntityByUuid('file', $uuid);

    // Check it.
    $this->assertEmpty($result->getFailures(), "There aren't any import errors.");
    $this->assertEquals($uuid, $file->uuid());
    $this->assertEquals(1, $file->getOwnerId());
    $this->assertEquals('drupal.txt', $file->filename->value);
    $this->assertEquals('vfs://root/' . static::VFS_FOLDER . '/drupal.txt', $file->uri->value);
    $this->assertEquals('text/plain', $file->filemime->value);
    $this->assertEquals(FILE_STATUS_PERMANENT, $file->status->value);
  }

}

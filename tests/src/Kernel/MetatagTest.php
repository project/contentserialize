<?php

namespace Drupal\Tests\contentserialize\Kernel;

use Drupal\contentserialize\Destination\FileDestination;
use Drupal\contentserialize\Source\FileSource;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\Node;
use Drupal\Tests\contentserialize\Traits\NodeKernelTestTrait;

/**
 * Provides tests for serializing metatags.
 *
 * @requires module metatag
 *
 * @group contentserialize
 */
class MetatagTest extends KernelTestBase {

  use NodeKernelTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['metatag', 'token'];

  /**
   * The metatag manager service.
   *
   * @var \Drupal\metatag\MetatagManagerInterface
   */
  protected $metatagManager;

  /**
   * The content serialize importer service.
   *
   * @var \Drupal\contentserialize\ImporterInterface
   */
  protected $importer;

  /**
   * The content serialize exporter service.
   *
   * @var \Drupal\contentserialize\ExporterInterface
   */
  protected $exporter;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->setUpNode();
    $this->installConfig(['metatag', 'system']);
    $this->createContentType(['type' => 'article']);

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_metatags',
      'entity_type' => 'node',
      'type' => 'metatag',
    ]);
    $field_storage->save();
    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'article',
    ])->save();

    $this->metatagManager = $this->container->get('metatag.manager');
    $this->importer = $this->container->get('contentserialize.importer');
    $this->exporter = $this->container->get('contentserialize.exporter');
    $this->entityRepository = $this->container->get('entity.repository');
  }

  /**
   * Test exporting and importing a node with metatags.
   */
  public function testExportImport() {
    $metatag_value = serialize(['title' => 'Custom Page Title']);
    $article = Node::create([
      'type' => 'article',
      'title' => 'Test Content',
      'body' => ['value' => 'Test Content Body', 'format' => 'basic_html'],
      'field_metatags' => ['value' => $metatag_value],
      'uid' => 1,
    ]);
    $article->save();

    // Export it.
    $destination = new FileDestination($this->getContentDirectory());
    $serialized = $this->exporter->exportMultiple([$article], 'json', ['json_encode_options' => JSON_PRETTY_PRINT]);
    $destination->saveMultiple($serialized);

    // Delete it.
    $uuid = $article->uuid();
    $article->delete();

    // Reimport it.
    $result = $this->importer->import(new FileSource($this->getContentDirectory()));

    /** @var \Drupal\Core\Entity\ContentEntityInterface $article */
    $article = $this->entityRepository->loadEntityByUuid('node', $uuid);

    // Check it.
    $this->assertEmpty($result->getFailures(), "There aren't any import errors.");
    $this->assertEquals($uuid, $article->uuid());
    $this->assertEquals($metatag_value, $article->field_metatags->value);
    $this->assertEquals('Test Content', $article->label());
    $this->assertEquals('Test Content Body', $article->body->value);
    $this->assertEquals(1, $article->uid->target_id);
  }

  /**
   * Test exporting and importing a node with defaults set.
   *
   * Test that exporting entities doesn't export the inherited metatags from
   * global and entity defaults.
   */
  public function testExportImportGlobalMetatags() {
    // Set the global title metatag.
    $this->container->get('config.factory')
      ->getEditable('metatag.metatag_defaults.global')
      ->set('tags.title', 'Welcome to the Test website')
      ->save();
    // Set the description metatag default for all nodes, and clear the title so
    // the global can override it.
    $this->container->get('config.factory')
      ->getEditable('metatag.metatag_defaults.node')
      ->set('tags.description', 'Website set up to perform various tests on.')
      ->clear('tags.title')
      ->save();
    // Create an article without any metatags.
    $article = Node::create([
      'type' => 'article',
      'title' => 'Test Content',
      'body' => ['value' => 'Test Content Body', 'format' => 'basic_html'],
      'uid' => 1,
    ]);
    $article->save();

    // Export the article.
    $destination = new FileDestination($this->getContentDirectory());
    $serialized = $this->exporter->exportMultiple([$article], 'json', ['json_encode_options' => JSON_PRETTY_PRINT]);
    $destination->saveMultiple($serialized);

    // Delete it.
    $uuid = $article->uuid();
    $article->delete();

    // Reimport it.
    $result = $this->importer->import(new FileSource($this->getContentDirectory()));
    $this->assertEmpty($result->getFailures(), "There aren't any import errors.");

    // Load it and ensure the metatags are empty.
    /** @var \Drupal\Core\Entity\ContentEntityInterface $article */
    $article = $this->entityRepository->loadEntityByUuid('node', $uuid);
    $this->assertEmpty($this->metatagManager->tagsFromEntity($article), "Imported article metatags are empty.");
  }

}

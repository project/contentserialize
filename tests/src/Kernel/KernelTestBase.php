<?php

namespace Drupal\Tests\contentserialize\Kernel;

use Drupal\KernelTests\KernelTestBase as CoreKernelTestBase;
use org\bovigo\vfs\vfsStream;

/**
 * Provides a base class for content serialization kernel tests.
 */
abstract class KernelTestBase extends CoreKernelTestBase {

  /**
   * The name of the virtual directory to store exported content in.
   */
  const VFS_FOLDER = 'content';

  /**
   * The modules required by all content serialization tests.
   */
  protected static $modules = [
    'system',
    'serialization',
    'vcsnormalizer',
    'bulkentity',
    'contentserialize',
  ];

  /**
   * A virtual directory to export to or import from.
   *
   * @var \org\bovigo\vfs\vfsStreamContent
   */
  protected $vfsContentDirectory;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->vfsContentDirectory = vfsStream::newDirectory(static::VFS_FOLDER)->at($this->vfsRoot);
    $this->installConfig(['contentserialize', 'bulkentity', 'serialization']);
  }

  /**
   * Get the virtual content directory.
   *
   * @return string
   *   The directory URL.
   */
  protected function getContentDirectory() {
    return $this->vfsContentDirectory->url();
  }

}

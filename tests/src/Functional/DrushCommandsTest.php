<?php

namespace Drupal\Tests\contentserialize\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drush\TestTraits\DrushTestTrait;

// @todo Remove when D8.7 is EOL.
// @see https://www.drupal.org/node/3041703
if (!trait_exists('\Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait')) {
  class_alias('\Drupal\Tests\taxonomy\Functional\TaxonomyTestTrait', '\Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait');
}

/**
 * Provides tests for the content serialization drush commands.
 *
 * @group contentserialize
 */
class DrushCommandsTest extends BrowserTestBase {

  use DrushTestTrait;
  use EntityReferenceTestTrait;
  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['contentserialize', 'node', 'filter', 'taxonomy'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem|object
   */
  protected $fs;

  /**
   * The path to export/import content to/from.
   *
   * @var string
   */
  protected $contentPath;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->contentPath = $this->tempFilesDirectory . '/content';
    $this->fs = $this->container->get('file_system');
    $this->assert($this->fs->mkdir($this->contentPath));
  }

  /**
   * Provides a simple smoke test for exporting and importing a node.
   */
  public function testExportImport() {
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);
    $uuid = '833587bb-c94c-4995-bcaf-4aee92ca45be';
    $node = $this->drupalCreateNode([
      'type' => 'page',
      'title' => 'Test Content',
      'body' => ['value' => 'Test content body', 'format' => 'basic_html'],
      'uuid' => $uuid,
      'uid' => 1,
      'status' => TRUE,
    ]);
    $old_nid = $node->id();
    $args = ['node', (string) $node->id()];
    $options = ['destination' => $this->contentPath];

    $this->drush('contentserialize:export', $args, $options);
    $node->delete();
    $this->drush('contentserialize:import', [], ['source' => $this->contentPath]);

    $repository = $this->container->get('entity.repository');
    /** @var \Drupal\node\NodeInterface $new_node */
    $new_node = $repository->loadEntityByUuid('node', $uuid);
    $this->assertSame($new_node->label(), 'Test Content');
    $this->assertSame($new_node->bundle(), 'page');
    $this->assertSame($new_node->body->value, 'Test content body');
    $this->assertSame($new_node->body->format, 'basic_html');
    $this->assertEquals($new_node->getRevisionUserId(), 1);
    $this->assert($new_node->isPublished());
    $this->assertNotEquals($new_node->id(), $old_nid);
  }

  /**
   * Tests exporting all entities with entity- and bundle-level exclusions.
   */
  public function testExportAll() {
    $uuids = [
      'node' => [
        'page' => '833587bb-c94c-4995-bcaf-4aee92ca45be',
        'article' => '90646bda-5ce6-4e3a-ab3b-5a85ccbcad3e',
      ],
      'term' => '9afe208b-4d9d-4111-bed0-4f137e61f945',
    ];
    // Create two content types to test excluding by bundle.
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);
    $this->drupalCreateNode([
      'type' => 'page',
      'uuid' => $uuids['node']['page'],
    ]);
    $this->drupalCreateNode([
      'type' => 'article',
      'uuid' => $uuids['node']['article'],
    ]);
    $vocabulary = $this->createVocabulary();
    $this->createTerm($vocabulary, ['uuid' => $uuids['term']]);

    // Execute the actual command. Exclude all users and article nodes.
    $options = [
      'destination' => $this->contentPath,
      'exclude' => 'node:article,user',
    ];
    $this->drush('contentserialize:export-all', [], $options);

    // Check the output.
    $actual = array_column($this->scanContentsDir(), 'name');
    $expected = [
      $uuids['node']['page'] . '.node',
      $uuids['term'] . '.taxonomy_term',
    ];
    // @todo When we're guaranteed at least PHPUnit 7.5 use
    //   \PHPUnit\Framework\Assert::assertEqualsCanonicalizing()
    sort($actual);
    sort($expected);
    $this->assertSame($actual, $expected);
  }

  /**
   * Tests exporting an entity and all its referenced entities.
   */
  public function testExportReferenced() {
    // Create a node that references another node. On exporting the host entity
    // there should also be the node from the ER field and the author.
    $uuids = [
      'node' => [
        'target' => '6afcc04b-e3b3-4712-ada2-c7fb341421d0',
        'host' => '02e1416d-fb2c-4d93-90e1-90c6159f79ab',
      ],
      'user' => '738e9c4f-4f14-4775-8aa5-608f845acf56',
    ];
    $user = $this->drupalCreateUser([], NULL, FALSE, ['uuid' => $uuids['user']]);
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);
    $this->createEntityReferenceField(
      'node',
      'page',
      'page_reference',
      'Related content',
      'node',
      'default',
      ['target_bundles' => ['page']],
      1
    );
    $target = $this->drupalCreateNode([
      'type' => 'page',
      'uid' => $user->id(),
      'uuid' => $uuids['node']['target'],
    ]);
    $host = $this->drupalCreateNode([
      'type' => 'page',
      'page_reference' => $target,
      'uid' => $user->id(),
      'uuid' => $uuids['node']['host'],
    ]);

    // Execute the command.
    $options = [
      'destination' => $this->contentPath,
    ];
    $this->drush('contentserialize:export-referenced', ['node', (string) $host->id()], $options);

    // Check the output.
    $actual = array_column($this->scanContentsDir(), 'name');
    $expected = [
      $uuids['node']['host'] . '.node',
      $uuids['node']['target'] . '.node',
      $uuids['user'] . '.user',
    ];
    // @todo When we're guaranteed at least PHPUnit 7.5 use
    //   \PHPUnit\Framework\Assert::assertEqualsCanonicalizing()
    sort($actual);
    sort($expected);
    $this->assertSame($actual, $expected);
  }

  /**
   * Returns all the files in the content directory.
   *
   * @return
   *   An associative array keyed by 'name' of objects with 'uri',
   *   'filename', and 'name' properties corresponding to the matched files.
   */
  protected function scanContentsDir() {
    return $this->fs->scanDirectory($this->contentPath, '/./');
  }

}

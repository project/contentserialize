<?php

namespace Drupal\Tests\contentserialize\Traits;

use Drupal\user\Entity\User;

/**
 * Provides a trait to facilitate working with users in a kernel test.
 */
trait UserKernelTestTrait {

  /**
   * Prepares the test for working with users.
   */
  protected function setUpUser() {
    $this->enableModules(['user']);
    $this->installEntitySchema('user');
    // Create a new user.
    $values = [
      'uid' => 1,
      'name' => $this->randomMachineName(),
    ];
    User::create($values)->save();
  }

}

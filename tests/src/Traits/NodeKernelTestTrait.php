<?php

namespace Drupal\Tests\contentserialize\Traits;

use Drupal\Tests\node\Traits\ContentTypeCreationTrait;

/**
 * Provides a trait to facilitate working with nodes in a kernel test.
 */
trait NodeKernelTestTrait {

  use ContentTypeCreationTrait;
  use UserKernelTestTrait;
  use TextFieldKernelTestTrait;

  /**
   * Prepares the test for working with nodes.
   */
  protected function setUpNode() {
    $this->setUpUser();
    $this->setUpTextField();

    $this->enableModules(['node']);
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('node');
    $this->installConfig(['node', 'filter']);
  }

}

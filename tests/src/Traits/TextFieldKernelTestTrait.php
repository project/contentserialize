<?php

namespace Drupal\Tests\contentserialize\Traits;

use Drupal\filter\Entity\FilterFormat;

/**
 * Provides a trait to facilitate working with text fields in a kernel test.
 */
trait TextFieldKernelTestTrait {

  /**
   * Prepares the test for working with test fields.
   */
  function setUpTextField() {
    $this->enableModules(['field', 'text', 'filter']);
    $this->installConfig(['filter']);

    FilterFormat::create([
      'format' => 'basic_html',
      'name' => 'Basic HTML',
    ])->save();

  }

}
